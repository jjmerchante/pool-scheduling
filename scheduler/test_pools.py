from django.test import TestCase
from .jobs import JobGit
from .pools import PoolGit
from .models import JobGit as JobGitModel

class Pool(TestCase):

    def setUp(self):
        self.pool = PoolGit()

    def test_insert_job(self):
        job = JobGit('owner1', 'repo1')
        self.pool.add(job)
        self.assertEqual(self.pool.count(), 1)
        job2 = self.pool.get_by_repo(repo='repo1')
        self.assertEqual(job2.data(),
                         {'owner': 'owner1', 'kind': 'git',
                          'repo': 'repo1'})

    def test_get_by_owner(self):
        repos = ['repo1', 'repo2', 'repo3', 'repo4', 'repo5']
        for repo in repos:
            job = JobGit('owner1', repo)
            self.pool.add(job)
        self.assertEqual(self.pool.count(), 5)
        jobs = self.pool.get_by_owner(owner='owner1', limit=3)
        print([job.data() for job in jobs])


class Job:
    """Root of the jobs hierarchy

    :param owner: owner of the job
    """

    def __init__(self, owner, birth=None):
        self.owner = owner
        self.birth = birth


class JobGit (Job):
    """
    Class for git jobs

    :param repo: repository identifier (url)

    """
    def __init__(self, owner, repo):
        self.repo = repo
        super().__init__(owner)

